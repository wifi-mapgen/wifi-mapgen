#!/usr/bin/perl
use strict;
use warnings;

my $includeTracks = 0;

my $hideCrypted = 1;
my $hideCloaked = 1;

my $usePoint = 1;

use Math::Trig;
use XML::LibXML;


sub htmlentities {
	local $_ = shift if @_;
	return '' unless defined;
	s/([^\w\s:])/'&#x'.(unpack 'H*',$1).';'/ge;
	$_
}

# BEGIN: code ported from kmlcircle-002

use constant Rad2Deg => 180 / pi;
use constant Deg2Rad => pi / 180;

# Mean Radius of Earth, meters
use constant MREarth => 6378.1 * 1000;

=pod
Convert (x,y,z) on unit sphere back to (long, lat)
=cut
sub xyz2LL {
	my ($x, $y, $z) = @_;
	
	my $long = $x ? atan($y / $x) : pi / 2;
	my $colat = acos($z);
	my $lat = (pi / 2 - $colat);
	
	# select correct branch of arctan
	$long = ($y <= 0) ? (-(pi - $long)) : (pi + $long) if $x < 0;
	
	($long * Rad2Deg, $lat * Rad2Deg)
}

=pod
convert long, lat IN RADIANS to (x,y,z)
=cut
sub LL2xyz {
	my ($long, $lat) = @_;
	
	my $theta = $long;
	# spherical coordinate use "co-latitude", not "lattitude"
	# lattiude = [-90, 90] with 0 at equator
	# co-latitude = [0, 180] with 0 at north pole
	my $phi = pi / 2 - $lat;
	
	my $x = cos($theta) * sin($phi);
	my $y = sin($theta) * sin($phi);
	my $z = cos($phi);
	
	($x, $y, $z)
}

=pod
rotate point pt, around unit vector vec by phi radians
http://blog.modp.com/2007/09/rotating-point-around-vector.html
=cut
sub rotPoint {
	my ($vec, $pt, $phi) = @_;
	
	# remap vector for sanity
	my ($u,$v,$w,$x,$y,$z) = ($vec->[0],$vec->[1],$vec->[2], $pt->[0],$pt->[1],$pt->[2]);
	
	my $a = $u*$x + $v*$y + $w*$z;
	my $d = cos($phi);
	my $e = sin($phi);
	
	(
		($a*$u + ($x - $a*$u)*$d + ($v*$z - $w*$y) * $e),
		($a*$v + ($y - $a*$v)*$d + ($w*$x - $u*$z) * $e),
		($a*$w + ($z - $a*$w)*$d + ($u*$y - $v*$x) * $e),
	)
}

=pod
polygonPoints -- get raw list of points in long,lat format for a regular polygon

offset = rotate by degrees

Returns a list of points comprising the object
=cut
sub polygonPoints {
	my ($long, $lat, $radius, $sides, $offset) = @_;
	$offset ||= 0;
	
	my $offsetRadians = $offset * Deg2Rad;
	# compute longitude degrees (in radians) at given latitude
	my $r = $radius / (MREarth * cos($lat * Deg2Rad));

	my @vec = LL2xyz($long * Deg2Rad, $lat * Deg2Rad);
	my @pt = LL2xyz($long * Deg2Rad + $r, $lat * Deg2Rad);
	
	my @pts;
	my $sidesPart = (2 * pi / $sides);
	for my $i (0..$sides) {
		push @pts, [xyz2LL(rotPoint(\@vec, \@pt, $offsetRadians + $sidesPart * $i))];
	}

	# connect to starting point exactly
	# not sure if required, but seems to help when
	# the polygon is not filled
	push @pts, $pts[0];
	(@pts)
}

# END: code ported from kmlcircle-002


sub loadNetXML {
	my ($data, $doc) = @_;
	if (not ref $doc) {
		$doc = XML::LibXML->load_xml(location => $doc);
	}
	my $xpdoc = XML::LibXML::XPathContext->new($doc);
	for my $wnet ($xpdoc->findnodes('//wireless-network')) {
		next unless $wnet->getAttribute('type') eq 'infrastructure';
		
		my $BSSID = $wnet->getElementsByTagName('BSSID');
		$BSSID = uc "$BSSID" if defined $BSSID;
		my $ESSID = $wnet->getElementsByTagName('essid');
		$ESSID = $wnet->getElementsByTagName('SSID') unless $ESSID;
		my $cloaked = ($ESSID ? $ESSID->[0]->getAttribute('cloaked') : undef) || $wnet->getAttribute('cloaked');
		$ESSID = "$ESSID" if defined $ESSID;
		
		my $crypt = $wnet->getElementsByTagName('encryption');
		$crypt = "$crypt" if defined $crypt;
		next if $hideCrypted and defined $crypt and $crypt !~ /None/;
		
		$cloaked = ($cloaked =~ /true/i) ? 1 : 0 if defined $cloaked;
		next if $hideCloaked and $cloaked;
		
		$data->{networks}->{$BSSID} = {
			BSSID => $BSSID,
			ESSID => $ESSID,
			cloaked => $cloaked,
			encryption => $crypt,
		};
	}
}

sub loadGPSXML {
	my ($data, $doc) = @_;
	
	my $GPSPoints = $data->{GPSPoints} ||= [];
	my $GPSPointsByBSSID = $data->{GPSPointsByBSSID} ||= {};
	my $GPSTrackLog = [];
	push @{ $data->{GPSTracks} ||= [] }, {
		trackLog => $GPSTrackLog,
	};
	
	if (not ref $doc) {
		$doc = XML::LibXML->load_xml(location => $doc);
	}
	my $xpdoc = XML::LibXML::XPathContext->new($doc);
	
	for my $point ($xpdoc->findnodes('//gps-point')) {
		my $info = {};
		for my $attr ($point->attributes()) {
			my ($n, $v) = ($attr->nodeName, $attr->value);
			$info->{$n} = $v;
		}
		my $BSSID = $info->{'bssid'};
		push @$GPSPoints, $info;
		if ($BSSID eq 'GP:SD:TR:AC:KL:OG') {
			push @$GPSTrackLog, $info;
		}
		else {
			$GPSPointsByBSSID->{$BSSID} ||= [];
			push @{$GPSPointsByBSSID->{$BSSID}}, $info;
		}
	}
}


sub kmlPlacemark {
	my %args = (@_);
	my $s;
	$s .= "\t<Placemark>\n";
	$s .= "\t\t<name>$args{name}</name>\n";
	$s .= "\t\t<description>$args{desc}</description>\n" if defined $args{desc};
	$s .= $args{content};
	$s .= "\t</Placemark>\n";
}

sub kmlLineStyle {
	my %args = (@_);
	my $s;
	$s .= "\t\t<Style>\n";
	$s .= "\t\t\t<LineStyle>\n";
	$s .= "\t\t\t\t<color>$args{color}</color>\n";
	$s .= "\t\t\t\t<width>$args{width}</width>\n";
	$s .= "\t\t\t</LineStyle>\n";
	$s .= "\t\t</Style>\n";
}

sub kmlLine {
	my %args = (@_);
	my $s;
	$s .= "\t\t<LineString>\n";
	$s .= "\t\t\t<coordinates>@{$args{coords}}</coordinates>\n";
	$s .= "\t\t</LineString>\n";
}

sub kmlPolygonStyle {
	my %args = (@_);
	my $s;
	$s .= "\t\t<Style>\n";
	$s .= "\t\t\t<PolyStyle>\n";
	$s .= "\t\t\t\t<$_>$args{$_}</$_>\n" for keys %args;
	$s .= "\t\t\t</PolyStyle>\n";
	$s .= "\t\t</Style>\n";
}

sub kmlPolygon {
	my %args = (@_);
	
	my $s;
	$s .= "\t\t<Polygon>\n";
	$s .= "\t\t\t<outerBoundaryIs>\n";
	$s .= "\t\t\t\t<LinearRing>\n";
	$s .= "\t\t\t\t\t<coordinates>@{$args{points}}</coordinates>\n";
	$s .= "\t\t\t\t</LinearRing>\n";
	$s .= "\t\t\t</outerBoundaryIs>\n";
	$s .= "\t\t</Polygon>\n";
}

=pod
Regular polygon

(longitude, latitude) in decimal degrees
meters is radius in meters
segments is number of sides, > 20 looks like a circle
offset, rotate polygon by a number of degrees

returns a string suitable for adding to a KML file.

You may want to edit this function to change "extrude" and other XML nodes.
=cut
sub kmlRegularPolygon {
	my ($long, $lat, $radius, $segments, $offset, $alt) = @_;
	$segments ||= 30;
	$offset ||= 0;
	$alt ||= 0;
	
	my @points =
		map { $_->[0] . ',' . $_->[1] . ',' . $alt }
		polygonPoints($long, $lat, $radius, $segments, $offset);
	
	kmlPolygon
		points => \@points,
}

sub kmlPoint {
	my %arg = (@_);
	my $s = '';
	$s .= "\t\t<Point>\n";
	$s .= "\t\t\t<coordinates>$arg{lon},$arg{lat},$arg{alt}</coordinates>\n";
	$s .= "\t\t</Point>\n";
}

sub netSort {
	my ($a, $b) = @_;
	my $aESSID = $a->{ESSID};
	$aESSID = undef if defined $aESSID and not length $aESSID;
	my $bESSID = $b->{ESSID};
	$bESSID = undef if defined $bESSID and not length $bESSID;
	if (defined $aESSID) {
		if (defined $bESSID) {
			# Both A and B have an ESSID
			return (uc $aESSID) cmp (uc $bESSID)
		}
		# Only A has an ESSID
		return -1
	}
	elsif (defined $bESSID) {
		# Only B has an ESSID
		return 1
	}
	# Neither A nor B have an ESSID
	(uc $a->{BSSID}) cmp (uc $b->{BSSID})
}

my $data = {};
while (my $filename = shift) {
	warn "Parsing: $filename\n";
	eval {
	if ($filename =~ /\.(?:net)?xml$/) {
		loadNetXML($data, $filename);
	}
	elsif ($filename =~ /\.gps(?:xml)?$/) {
		loadGPSXML($data, $filename);
	}
	else {
		warn "wtf is $filename?\n";
	}
	};
	warn "Error: $@" if $@;
}
warn "Finished parsing inputs\n";

for my $BSSID (keys %{$data->{GPSPointsByBSSID}}) {
	my $net = $data->{networks}->{$BSSID};
	if (not defined $net) {
		delete $data->{GPSPointsByBSSID}->{$BSSID};
		next
	}
	my $ESSID = $net->{ESSID};
	$net->{Title} = $ESSID || '[ [ [ ' . $BSSID . ' ] ] ]';
}

print "<?xml version='1.0' encoding='UTF-8'?>\n";
print "<kml xmlns='http://earth.google.com/kml/2.0'>\n";
print "\t<Document>\n";
if ($includeTracks) {
print "\t\t<Folder>\n";
print "\t\t\t<name>Tracks</name>\n";
for my $track (@{$data->{GPSTracks}}) {
#	my $last;
	print
	kmlPlacemark
		name => 'Route',
		desc => 'gpsd track log',
		content =>
			kmlLineStyle(
				color => 'BB0000FF',
				width => 1,
			) .
			kmlLine(
				coords => [
					map { $_->{'lon'} . ',' . $_->{'lat'} . ',' . ($_->{'alt'} || 0) }
#					grep {  }
					sort { $a->{'time-sec'} <=> $b->{'time-sec'} or $a->{'time-usec'} <=> $b->{'time-usec'} }
					grep { $_->{'lon'} }
					@{$track->{trackLog}}
				],
			),
	;
}
print "\t\t</Folder>\n";
}
print "\t\t<Folder>\n";
print "\t\t\t<name>Networks</name>\n";
for my $BSSID (
	sort { netSort $data->{networks}->{$a}, $data->{networks}->{$b} }
	keys %{$data->{GPSPointsByBSSID}}
) {
	my $net = $data->{networks}->{$BSSID};
	my $ESSID = $data->{networks}->{$BSSID}->{ESSID};
	
	my $points = $data->{GPSPointsByBSSID}->{$BSSID};
	my %summary = (
		latMin => 99999,
		latMax => -99999,
		lonMin => 99999,
		lonMax => -99999,
	);
	for my $point (@$points) {
		$summary{latMin} = $point->{lat} if $point->{lat} < $summary{latMin};
		$summary{latMax} = $point->{lat} if $point->{lat} > $summary{latMax};
		$summary{lonMin} = $point->{lon} if $point->{lon} < $summary{lonMin};
		$summary{lonMax} = $point->{lon} if $point->{lon} > $summary{lonMax};
	}
	$summary{latRad} = ($summary{latMax} - $summary{latMin}) / 2;
	$summary{latC} = $summary{latMin} + $summary{latRad};
	$summary{lonRad} = ($summary{lonMax} - $summary{lonMin}) / 2;
	$summary{lonC} = $summary{lonMin} + $summary{lonRad};
	$summary{rad} = $summary{latRad} < $summary{lonRad} ? $summary{latRad} : $summary{lonRad};
	$summary{rad} = 10 if $summary{rad} < 10;
	
	my $name = $net->{Title};
	my $desc = "BSSID: $BSSID";
	$desc .= "<br>ESSID: $ESSID" if defined $ESSID;
	$desc .= "<em> (cloaked)</em>" if $net->{cloaked};
	{ my $crypt = $net->{encryption}; $desc .= "<br>Encryption: $crypt" if defined $crypt; }
	
	$desc = "<![CDATA[$desc]]>" if $desc =~ /[<>]/;
	
	print
	kmlPlacemark
		name => htmlentities($name),
		desc => $desc,
		content =>
			$usePoint ? (
				kmlPoint(
					'lat' => $summary{latC},
					'lon' => $summary{lonC},
					'alt' => $points->[0]->{alt},
				),
			) : (
			kmlLineStyle(
				color => 'BB0000FF',
				width => 2,
			) .
			kmlPolygonStyle(
				fill => 1,
				color => 'BB0000FF',
				outline => 1,
			) .
			kmlRegularPolygon(
				$summary{lonC},
				$summary{latC},
				$summary{rad},
				10,
				undef,
				undef,
				$points->[0]->{alt},
			),
			),
	;
}
print "\t\t</Folder>\n";
print "\t</Document>\n";
print "</kml>\n";
